# Unity-first-proyect

Explicación del juego: juego de aventura, entorno bosque. Finalidad: recoger gemas y recorrer un mundo con distintos efectos especiales. 

Desarrollo: se ha diseñado un terreno con diferentes tipos de arboles, cielo, texturas de agua, suelo, puentes, montañas, plataformas, etc.
El juego contiene los siguientes assets descargados gratuitamente:
    3DForce
    3Skyboxes
    Campfire Pack
    Cardboard
    Christmas-Presents
    Editor
    GrassRoadRace
    Horse
    Medieval House
    Medieval:LowPoly_Carts_Free
    Mesh
    PainterlyNature
    Party Pack
    PowerUps Vol 1 Free
    Standard Assets
    Town Creator Kit LITE
    Water FX Particles
    Wispy Sky

Además, se han utilizado diferentes efectos especiales como son:
Viento, uso de luces en el juego, decoración.
Sistema de particulas:
    Efectos fisicos para simular movimiento de pelota, animaciones para caballos, joyas, fuegos artificiales(Firework), niebla (Duststorm), hoguera (Flares), chimenea (AfterBurning), Simulacion de 'fuentes' (WaterSplash) , etc.
Musica:
    Se ha añadido una musica principal para el mundo y otra para simular los pasos del character.
Modo de uso:
    El juego puede ser utilizado en 3 persona mediante el personaje Ethan o en primera persona con Cardboard, uso de gafas 3D.
    
Programación:
Se ha trabajado en el reconocimiento de joyas y tratamiento para puntuar. Animaciones. 

Spyros soundtracks:
https://www.youtube.com/watch?v=PEM-pYocKnI&list=PL8F255D9CF7D7A1E0&index=2

![Game image example](images/house.PNG)
