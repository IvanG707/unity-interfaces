﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class ColisionScript : MonoBehaviour {
    public GameObject goText;
    public int puntuacion = 0;
    Text texto;
    
    // Use this for initialization
    void Start () {
        puntuacion = 0;
        
    }
	
	// Update is called once per frame
	void Update () {
        goText.GetComponent<Text>().text = "Score : " + puntuacion + "/60";
    }

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "diamante")
        {
            
            Destroy(col.gameObject);
            IncrementarPuntuacion(1);
            
        }
    }

    public void IncrementarPuntuacion(int valor)
    {
        puntuacion += valor;
        
    }

    

    public void RespawnPlayer()
    {
        puntuacion = 0;
        
    }
}
 
